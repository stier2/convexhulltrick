# convexhulltrick

This is a small python package containing a C++ implementation of the convex hull trick to efficiently find the minimum of a set of n affine functions:

$$
f(x) := \min_i s_i \cdot x + a_i
$$

It currently provides only one function `cht_sorted(slopes, offsets, order, xmin, xmax)`.

Parameters:
- `slopes = np.array(n)` contining slopes $s_i$ of type double or float
- `offsets = np.array(n)` contining offsets $a_i$ of type double or float
- `order = np.array(n, dtype=np.int32)` such that `slopes[order[...]]` is ordered non-increasingly as you would get e.g. from calling `np.argsort(slopes)[::-1].astype(np.int32)`
- `xmin = float` defining the start of the domain (default `-inf`)
- `xmax = float` defining the end of the domain (default `inf`)

Returns:
- `indices = np.array(n, dtype=np.int32)` 
- `points = np.array(n+1)` such that from `points[i]` to `points[i+1]` the function with index `indices[i]` is minimal.

Note that this function will be most efficient when the input arrays have the right types and are stored contiguously in memory. In that case, no copies will be made.


## Installation

Clone the repository, initialize the pybind11 submodule and install using pip with the commands below:

```
git clone https://gitlab.gwdg.de/stier2/convexhulltrick.git
cd convexhulltrick
git submodule update --init
pip install .
```


## License
MIT License
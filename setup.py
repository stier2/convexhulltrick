from pybind11.setup_helpers import Pybind11Extension, build_ext
from setuptools import setup
import sysconfig

extra_compile_args = sysconfig.get_config_var('CFLAGS').split()
extra_compile_args += ["-O3", "-Wall"]

ext_modules = [
    Pybind11Extension(
        "convexhulltrick",
        ["src/main.cpp"],
        include_dirs = ["pybind11/include"],
        cxx_std = 20,
        extra_compile_args = extra_compile_args,
    ),
]

setup(
    ext_modules=ext_modules,
    zip_safe=False
)

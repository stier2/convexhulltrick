#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/numpy.h>
#include <vector>
#include <algorithm>
#include <cmath>

namespace py = pybind11;

#define RESERVE_SIZE 50
#define DO_INPUT_CHECKS


// find min_i slopes[i] * x + offsets[i] within the range xmin to xmax in linear time
template <typename I = int, typename T = double>
//std::pair<std::vector<I>, std::vector<T>>
std::pair<py::array_t<I>, py::array_t<T>>
cht_sorted(const py::array_t<T, py::array::c_style | py::array::forcecast> &slopes_np,
           const py::array_t<T, py::array::c_style | py::array::forcecast> &offsets_np,
           const py::array_t<I, py::array::c_style | py::array::forcecast> &order_np,
           const T &xmin, const T &xmax) {

  py::buffer_info slopes_buf = slopes_np.request(), offsets_buf = offsets_np.request(), order_buf = order_np.request();

#ifdef DO_INPUT_CHECKS
  if (slopes_buf.ndim != 1 || offsets_buf.ndim != 1 || order_buf.ndim != 1)
    throw std::runtime_error("input arrays must be one dimensional");

  if (slopes_buf.size != offsets_buf.size || slopes_buf.size != order_buf.size)
    throw std::runtime_error("input arrays must be of the same size");

  if (xmin > xmax)
    throw std::runtime_error("xmin has to be smaller than xmax");
#endif

  T *slopes = static_cast<T*>(slopes_buf.ptr);
  T *offsets = static_cast<T*>(offsets_buf.ptr);
  I *order = static_cast<I*>(order_buf.ptr);
  I n = slopes_buf.size;
  
  I maxi = 0;
  T maxy = slopes[order[maxi]] * xmin + offsets[order[maxi]], y;
  if (std::isfinite(xmin)) {
    for (int i = 1; i < n; i++) {

#ifdef DO_INPUT_CHECKS
      if (slopes[order[i]] > slopes[order[i-1]])
        throw std::runtime_error("slopes are not ordered");
#endif
      
      if ((y = slopes[order[i]] * xmin + offsets[order[i]]) < maxy)
        maxy = y, maxi = i;
      
    }
  }
#ifdef DO_INPUT_CHECKS
  else {
    for (int i = 1; i < n; i++)
      if (slopes[order[i]] > slopes[order[i-1]])
        throw std::runtime_error("slopes are not ordered");
  }
#endif
  
  std::vector<I> is({order[maxi]});
  std::vector<T> xs({xmin, xmin});

  is.reserve(RESERVE_SIZE);
  xs.reserve(RESERVE_SIZE);

  for (int i = maxi+1; i < n; ++i) {
    
    if (!is.size()) {
      // if there is no previous line -> add this one
      is.push_back(order[i]);
      xs.push_back(xmin);
      continue;
    }

    I pi = is.back();
    I oi = order[i];

    T ds = (slopes[pi] - slopes[oi]);
    //T ds = (slopes(pi) - slopes(i));
    if (ds == 0) {
      // lines are parallel
      if (offsets[oi] < offsets[pi]) {
      //if (offsets(i) > offsets(pi)) {
        // new line is lower -> remove last and continue with same
        is.pop_back();
        xs.pop_back();
        --i;
      }
      continue;
    }

    // calculate intersect
    T x = (offsets[oi] - offsets[pi]) / ds;
    //T x = (offsets(i) - offsets(pi)) / ds;

    if (x > xmax) {
      // if intersect is above end of region continue with next
      continue;
    }
    
    if (x < xs[xs.size() - 2]) {
      // intersect is before previous segment started -> remove last and continue with same
      is.pop_back();
      xs.pop_back();
      --i;
      continue;
    }
    
    // set end of previous segment and add new one
    xs.back() = x;
    is.push_back(oi);
    xs.push_back(x);
  }
  xs.back() = xmax;

  auto is_np = py::array_t<I>(is.size());
  auto xs_np = py::array_t<T>(xs.size());
  
  py::buffer_info is_buf = is_np.request();
  py::buffer_info xs_buf = xs_np.request();

  std::copy(is.begin(), is.end(), static_cast<I*>(is_buf.ptr));
  std::copy(xs.begin(), xs.end(), static_cast<T*>(xs_buf.ptr));
  
  return {is_np, xs_np};
}


PYBIND11_MODULE(convexhulltrick, m) {

  std::string cht_sorted_description =
    "Find the minimum of a selection of n affine linear functions f_i(x) = slopes[i] * x + offsets[i] within the domain [xmin, xmax]\n\n"
    "Parameters:\n"
    "slopes = numpy array of function slopes\n"
    "offsets = numpy array of function offsets\n"
    "order = integer numpy array such that slopes[order[...]] is non-increasing (largest first)\n"
    "xmin = lower bound of domain (default -inf)\n"
    "xmax = upper bound of domain (default inf)\n\n"
    "Returns:\n"
    "a pair containing (indices, changepoints) such that between changepoints[i] and changepoints[i+1] the function f_indices[i] is maximal";
  
  m.def("cht_sorted", &cht_sorted<int,double>,
        py::arg("slopes"),
        py::arg("offsets"),
        py::arg("order"),
        py::arg("xmin") = -std::numeric_limits<double>::infinity(),
        py::arg("xmax") = std::numeric_limits<double>::infinity(),
        cht_sorted_description.c_str());

  m.def("cht_sorted", &cht_sorted<int,float>,
        py::arg("slopes"),
        py::arg("offsets"),
        py::arg("order"),
        py::arg("xmin") = -std::numeric_limits<double>::infinity(),
        py::arg("xmax") = std::numeric_limits<double>::infinity(),
        cht_sorted_description.c_str());
}



